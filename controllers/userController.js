const User = require('./../models/userModels')
exports.getAllUser = async(req, res, next) => {
    try{
        const user = await User.find()
        res.status(200).json({data: user,status: 'success'})
    }
    catch (err) {
        res.status(500).jason({error: err.message});
    }
}

exports.createUser = async(req, res, ) => {
    try{
        const user = await User.create(req.body);
        console.log(req.body.name)
        res.json({data: user,status: 'sucess'})
    }
    catch (err) {
        res.status(500).jason({error: err.message});
    }
}

exports.getUser = async(req, res, ) => {
    try{
        const user = await User.findById(req.paramas.id);
        res.json({data: user,status: 'sucess'})
    }
    catch (err) {
        res.status(500).jason({error: err.message});
    }
}
exports.updateUser = async(req, res, ) => {
    try{
        const user = await User.findByIdAndUpdate(req.paramas.id,req.body);
        res.json({data: user,status: 'sucess'})
    }
    catch (err) {
        res.status(500).jason({error: err.message});
    }
}
exports.deleteUser = async(req, res, ) => {
    try{
        const user = await User.findByIdAndUpdate(req.paramas.id);
        res.json({data: user,status: 'sucess'})
    }
    catch (err) {
        res.status(500).jason({error: err.message});
    }
}