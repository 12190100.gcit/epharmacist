const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: [true, 'please tell you name!'],
    },
    email:{
        type: String,
        required: [true, 'please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail,'please provide a valid email'],
    },
    photo: {
        type: String,
        default: 'default.jpg',
    },
    password: {
        type: String,
        required: [true, 'please provide a password'],
        minlength: 8,
        select: false,
    },
    active: {
        type: Boolean,
        default: true,
        select: false,
    },
    role: {
        type: String,
        enum: ['user', 'sme','pharmacist', 'admin'],
        default: 'user',

    },
    passwordConfirm:{
        type: String,
        required:[true,'please confirm your password'],
        validate: {
            validator: function(el){
                return el === this.password
            },
            message: 'password are not the same',
        },
    },
   

})
userSchema.pre('save', async function (next){
    if(!this.isModified('password'))return next()
    this.password = await bcrypt.hash(this.password, 12)
    this.passwordConfirm =undefined
    next()
})
userSchema.methods.correctPassword = async function(
    candidatePassword,
    userPassword,
){
    return await bcrypt.compare(candidatePassword, userPassword)
}
const User = mongoose.model('User', userSchema)
module.exports = User;
